var roll = 0;
var totalMoney = new Array();
var highestMoney = 0;
var highestRoll = 1;
var firstBet;


function inputCheck (betAmount) {
	if (betAmount <= 0 || isNaN(betAmount) == true || betAmount>=500){
		alert("Your bet should be at least a dollar and less than 500.")
		return false;
	}
	else {
		return true;
	}
}

function dieRoll () {
    var rollOne = 1 + (Math.floor(Math.random() * 6));
    var rollTwo = 1 + (Math.floor(Math.random() * 6));
    return rollOne + rollTwo;
}


function setTheBet () {
	if (inputCheck(document.getElementById("startingBet").value) == true){
	betAmount = Number(document.getElementById("startingBet").value);
	highestMoney = betAmount;
	firstBet = betAmount
	console.log("The first bet was " + betAmount);
	playTheGame();
	}
}

function playTheGame () {

		while(betAmount > 0) {
			var result = dieRoll();
			console.log("the roll was " + result);
			roll++;
			if(result == 7){
			betAmount += 4;
			console.log("A win--the money stands at " + betAmount);
				if(betAmount>highestMoney){
					highestMoney = betAmount;
					highestRoll = roll;
				}
			}
			else{
			betAmount -= 1;
			console.log("A loss--the money stands at " + betAmount);
			}
			totalMoney.push(betAmount);
			console.log(totalMoney);
			console.log("You rolled " + totalMoney.length + " times");
			console.log("The value of roll is " + roll);
			console.log("The most money you had was " + Math.max(...totalMoney));
			console.log("The most money you had was " + (highestMoney - 1) );
			console.log("You had the most money on roll #" + highestRoll);
		}
		
		outputResults();
};

function outputResults() {
	document.getElementById("startingBetTable").textContent = "$" + firstBet;
	document.getElementById("amountWonTable").textContent = "$" + Math.max(...totalMoney);
	document.getElementById("broke").textContent = totalMoney.length;
	document.getElementById("rollCount").textContent = highestRoll;
	document.getElementById("enteredBet").setAttribute('value', 'Play Again');
	document.getElementById("startingBet").value="";
	
	console.log("function was called");


}

