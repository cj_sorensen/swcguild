var errors = new Array();

function checkForm(){
	// Reset errors secion
	errors = [];
	document.getElementById("errors").innerHTML = "";
	//call individual checks
	isNameFilled();
	hasEmailOrPhone();
	infoCompleted();
	areDaysChecked();

	//functions for checking each input state
	function isNameFilled() {
		if (document.getElementById("name").value == ""){
			errors.push("Please provide your name.");
			console.log(errors);
			return false;
		}
	}

	function hasEmailOrPhone() {
		if (document.getElementById("phone").value == "" && document.getElementById("email").value == "") {
			errors.push("Please provide an email or phone number.");
			console.log(errors);
			return false;
		}
	}

	function infoCompleted() {
		if (document.getElementById("reason").value == "Other" && document.getElementById("additional").value == "") {
			errors.push("Please provide additional information about your inquiry.");
			console.log(errors);
			return false;
		}
	}

	function areDaysChecked() {
		if (document.getElementById("monday").checked == false && document.getElementById("tuesday").checked == false && document.getElementById("wednesday").checked == false&& document.getElementById("thursday").checked == false && document.getElementById("friday").checked == false){
			errors.push("Please check a day you can be reached.");
			console.log(errors);
			return false;
		}
	}
	//check to see if there are errors in the array and then loop through and output them to the page
	if (errors.length > 0){
		for (var i=0; i<errors.length; i++){
			document.getElementById("errors").innerHTML += errors[i] + "<br>";
		}
	}
	//Send user to a thank you page
	else {
		window.location.href ="thankYou.html";

	}
} 





